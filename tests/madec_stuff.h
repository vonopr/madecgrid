#pragma once

#include "madec.h"

const int prec = 40;
MadecPlane mp(prec);


TEST(madec, equator){
  double njeq = madec::njeq;
  mp.calc(njeq);

  ASSERT_DOUBLE_EQ(njeq, to_double(mp.njeq));

  ASSERT_DOUBLE_EQ(0.0, madec::fdeg(njeq));
  ASSERT_DOUBLE_EQ(0.0, madec::gdeg(njeq));

  ASSERT_DOUBLE_EQ(0.0, to_double(mp.fdeg));
  ASSERT_DOUBLE_EQ(0.0, to_double(mp.gdeg));
  
  ASSERT_DOUBLE_EQ(1.0, madec::rad(njeq));
  ASSERT_DOUBLE_EQ(1.0, to_double(mp.yr));
}

TEST(madec, pole){
  double jnpole = madec::jnpole;
  mp.calc(jnpole);

  ASSERT_NEAR(90.0, madec::fdeg(jnpole), 1.0e-4);
  ASSERT_NEAR(90.0, to_double(mp.fdeg), 1.0e-4);
  
  ASSERT_NEAR(0.0, madec::rad(jnpole), 1.0e-4);
  ASSERT_NEAR(0.0, to_double(mp.yr), 1.0e-4);
}



const double delta_001 = 0.001;
const double eps_001 = 0.001;

double const_1(double x){return 1.0;}
double x2(double x){return x*x;}
double sinx(double x){return sin(x);}


TEST(madec, diff){
  const double x0 = 0.0;
  const double x10 = 10.0;
  
  ASSERT_NEAR(0.0, diff(const_1, x0, delta_001), eps_001);
  ASSERT_NEAR(0.0, diff(const_1, x10, delta_001), eps_001);

  ASSERT_NEAR(0.0, diff(x2, x0, delta_001), eps_001);
  ASSERT_NEAR(20.0, diff(x2, x10, delta_001), eps_001);

  ASSERT_NEAR(cos(0.0), diff(sinx, x0, delta_001), eps_001);
  ASSERT_NEAR(cos(10.0), diff(sinx, x10, delta_001), eps_001);
}

double i00 = 35.0;
double j00 = 95.0;

TEST(madec, fgdeg_j){
  mp.calc(j00);
  
  ASSERT_NEAR(madec::fdeg_j(j00), diff(madec::fdeg, j00, 1.0e-3), 1.0e-2);
  ASSERT_NEAR(1.0, madec::fdeg_j(j00)/diff(madec::fdeg, j00, 1.0e-3), 1.0e-4);
  
  ASSERT_NEAR(madec::gdeg_j(j00), diff(madec::gdeg, j00, 1.0e-3), 1.0e-2);
  ASSERT_NEAR(1.0, madec::gdeg_j(j00)/diff(madec::gdeg, j00, 1.0e-3), 1.0e-4);

  ASSERT_NEAR(madec::fdeg_j(j00), to_double(mp.fdeg_j), 1.0e-2);
  ASSERT_NEAR(1.0, madec::fdeg_j(j00)/to_double(mp.fdeg_j), 1.0e-4);

  ASSERT_NEAR(madec::gdeg_j(j00), to_double(mp.gdeg_j), 1.0e-2);
  ASSERT_NEAR(1.0, madec::gdeg_j(j00)/to_double(mp.gdeg_j), 1.0e-4);
}

TEST(madec, fgdeg_jj){
  ASSERT_NEAR(madec::fdeg_jj(j00), diff(madec::fdeg_j, j00, 1.0e-3), 1.0e-2);
  ASSERT_NEAR(1.0, madec::fdeg_jj(j00)/diff(madec::fdeg_j, j00, 1.0e-3), 1.0e-4);

  ASSERT_NEAR(madec::gdeg_jj(j00), diff(madec::gdeg_j, j00, 1.0e-3), 1.0e-2);
  ASSERT_NEAR(1.0, madec::gdeg_jj(j00)/diff(madec::gdeg_j, j00, 1.0e-3), 1.0e-4);
}


TEST(madec, yplane_deg2){
  ASSERT_NEAR(madec::yplane_deg2(j00), diff(madec::yplane_deg, j00, 1.0e-3), 1.0e-3);
  ASSERT_NEAR(1.0, madec::yplane_deg2(j00)/diff(madec::yplane_deg, j00, 1.0e-4), 1.0e-4);
}
TEST(madec, yc_j){
  ASSERT_NEAR(madec::yc_j(j00), diff(madec::yc, j00, 1.0e-3), 1.0e-3);
  ASSERT_NEAR(1.0, madec::yc_j(j00)/diff(madec::yc, j00, 1.0e-4), 1.0e-4);
}
TEST(madec, yc_jj){
  ASSERT_NEAR(madec::yc_jj(j00), diff(madec::yc_j, j00, 1.0e-3), 1.0e-3);
  ASSERT_NEAR(1.0, madec::yc_jj(j00)/diff(madec::yc_j, j00, 1.0e-4), 1.0e-4);
}

double i00j_to_phi(double j) { return madec::phi_plane(madec::i_to_phi0(i00), j, mp); };
double i00j_to_phi_j(double j) { return madec::phi_j(i00, j, madec::phi_exp_arg(j, mp), mp); };
TEST(madec, phi_j){
  ASSERT_NEAR(i00j_to_phi_j(j00), diff(i00j_to_phi, j00, 1.0e-2), 1.0e-3);
  ASSERT_NEAR(1.0, i00j_to_phi_j(j00)/diff(i00j_to_phi, j00, 1.0e-5), 1.0e-4);
}

double ij00_to_phi(double i) { return madec::phi_plane(madec::i_to_phi0(i), j00, mp); };
double ij00_to_phi_i(double i) { return madec::phi_i(i, madec::phi_exp_arg(j00, mp), mp); };
TEST(madec, phi_i){
  ASSERT_NEAR(ij00_to_phi_i(i00), diff(ij00_to_phi, i00, 1.0e-2), 1.0e-3);
  ASSERT_NEAR(1.0, ij00_to_phi_i(i00)/diff(ij00_to_phi, i00, 1.0e-5), 1.0e-4);
}

void grid_at_ij(
    double &lon, double &lon_i, double &lon_j,
    double &lat, double &lat_i, double &lat_j,
    double i, double j
  ){
    double lona[1], lata[1];
    double lona_i[1], lona_j[1], lata_i[1], lata_j[1];
    double iarr[1];
    double jarr[1];
    int ni = 1;
    int nj = 1;
    
    iarr[0] = i;
    jarr[0] = j;

    madec::ij_to_lonlat(lona, lona_i, lona_j, lata, lata_i, lata_j, iarr, jarr, ni, nj, mp);
    
    lon = lona[0];
    lon_i = lona_i[0];
    lon_j = lona_j[0];

    lat = lata[0];
    lat_i = lata_i[0];
    lat_j = lata_j[0];
  }

double ij00_to_lon(double i){
  double lon;
  double dummy; 
  grid_at_ij(lon, dummy, dummy, dummy, dummy, dummy, i, j00);
  return lon;
}
double ij00_to_lon_i(double i){
  double lon_i;
  double dummy; 
  grid_at_ij(dummy, lon_i, dummy, dummy, dummy, dummy, i, j00);
  return lon_i;
}
double i00j_to_lon(double j){
  double lon;
  double dummy; 
  grid_at_ij(lon, dummy, dummy, dummy, dummy, dummy, i00, j);
  return lon;
}
double i00j_to_lon_j(double j){
  double lon_j;
  double dummy; 
  grid_at_ij(dummy, dummy, lon_j, dummy, dummy, dummy, i00, j);
  return lon_j;
}
double ij00_to_lat(double i){
  double lat;
  double dummy; 
  grid_at_ij(dummy, dummy, dummy, lat, dummy, dummy, i, j00);
  return lat;
}
double ij00_to_lat_i(double i){
  double lat, lat_i;
  double dummy; 
  grid_at_ij(dummy, dummy, dummy, lat, lat_i, dummy, i, j00);
  return lat_i;
}
double i00j_to_lat(double j){
  double lat;
  double dummy; 
  grid_at_ij(dummy, dummy, dummy, lat, dummy, dummy, i00, j);
  return lat;
}
double i00j_to_lat_j(double j){
  double lat_j;
  double dummy; 
  grid_at_ij(dummy, dummy, dummy, dummy, dummy, lat_j, i00, j);
  return lat_j;
}
TEST(madec, lonlat_ij){
  const double delta0_001 = 0.001;
  const double eps0_01 = 0.01;
  ASSERT_NEAR(1.0, diff(ij00_to_lon, i00, 1.0e-2)/ij00_to_lon_i(i00), 1.0e-2);
  ASSERT_NEAR(1.0, diff(i00j_to_lon, j00, 1.0e-2)/i00j_to_lon_j(j00), 1.0e-2);
  ASSERT_NEAR(1.0, diff(ij00_to_lat, i00, 1.0e-2)/ij00_to_lat_i(i00), 1.0e-2);
  ASSERT_NEAR(1.0, diff(i00j_to_lat, j00, 1.0e-2)/i00j_to_lat_j(j00), 1.0e-2);
}
