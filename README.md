# madecgrid

Realization of [global orthogonal curvilinear grid for ocean by Madec, G. and Imbard](https://link.springer.com/article/10.1007%2FBF00211)

## Dependencies 

[ARB](https://arblib.org),
 
and ARB's dependecies:
 - [MPIR](http://www.mpir.org) 2.6.0 or later, or [GMP](http://www.gmplib.org) 5.1.0 or later
 - [MPFR](http://www.mpfr.org) 3.0.0 or later
 - [FLINT](http://www.flintlib.org) version 2.5 or later. Possibly better to git checkout verion from [repository of arb's author](https://github.com/fredrik-johansson/flint2)

## Installtion

The simplest way to install dependencies is to use [Homebrew package manager](https://brew.sh).
If brew is installed and is on your system path, just execute

```
brew install arb
```

If you installed brew to your home directory path for arb and flint installtion would be `$HOME/.linuxbrew/Cellar/arb/<arb-version>` and `$HOME/arb/<flint-version>`.


Define variables containing paths to arb and flint directories.
```bash
export ARB_DIR=*path to arb installation directory*
export FLINT_DIR=*path to flint installation directory*
```

Download sources of this project from git

```bash
git clone https://gitlab.com/vonopr/madecgrid.git
```

Configure and build it with cmake:

```bash
mkdir build
cmake -S ./madecgrid -B ./build -DCMAKE_PREFIX_PATH="$ARB_DIR;$FLINT_DIR" -DBUILD_TESTING=yes
cmake --build ./build
cd build
ctest
```
To avoid testing do not set BUILD_TESTING variable and skip two last lines.

The installation path can be set with CMAKE_INSTALL_PREFIX after building.
```bash
cmake -DCMAKE_INSTALL_PREFIX=*your installation path* .
cmake --install ./build
```

## Run

Go to the installtion path and run the program
```bash
cd *your installation path*
./bin/madecgrid -n 361
```

The program  will produce the file "madec_grid.csv" with grid of 361×361 size.



## A hint to install arb with Julia 

Install [julia](https://julialang.org), install [nemo](http://nemocas.github.io/Nemo.jl/latest/) package. Needed includes and libaries will be installed,
the typical destination path is `~/.julia/artifacts`. Find *flint.h* in `~/.julia/artifacts/*/include`. Copy this directory to *some place* and rename it to *flint*.
Find *arb.h* in `~/.julia/artifacts/*/include`. Copy this directory to directory *that place*(directory containing flint) and rename it to *arb*. 
You should obtain directory with following content

```ascii
.
├── arb
│   ├── include
│   ├── lib
│   ├── logs
│   └── share
└── flint
    ├── include
    ├── lib
    ├── logs
    └── share
```


If you obtained ARB using hint with julia package set the path to directory with *flint* and *arb* with

```bash
export DEPS_DIR=*path to your directory with flint and arb*
```

then

```bash
export ARB_DIR=$DEPS_DIR/arb
export FLINT_DIR=$DEPS_DIR/flint
```

do the rest steps from the main installtion section.
