#include "madec.h"
#include "cmd-parse.h"
#include "geodesic.h"

int main(int argc, char* argv[]) {

  // define default values for cmd arguments
  std::string oString    = "Default Value";
  int nx = 21;
  int print_help = 0;

  // set non-adjustable parameters
  auto constexpr filename = "madec_grid.csv";

  // configure possible command line options.
  CommandLine args("A demonstration of the simple command line parser.");
  args.addArgument({"-n", "--nx"},  &nx,  "Length of grid axes");
  args.addArgument({"-h", "--help"}, &print_help, "Print help");

  // parse arguments
  try {
    args.parse(argc, argv);
  } catch (std::runtime_error const& e) {
    std::cout << e.what() << std::endl;
    return -1;
  }

  // print a help message and exit.
  if (print_help) {
    args.printHelp();
    return 0;
  }


  // print the resulted configuration
  std::cout << "nx: "  << nx  << std::endl;
  std::cout << "filename: "  << filename  << std::endl;


  // write grid to csv file
  std::string grid_file(filename);
  madec::write_grid(grid_file, nx, nx);
  
  std::string geod_grid_file{"geodesic.csv"};
  geodesic::write_grid(geod_grid_file, nx, nx);

  return 0;
}

