#pragma once
#include <iostream>
#include <numeric>
#include <cmath>
#include <vector>
#include <typeinfo> 
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <iomanip> 
#include <bits/stdc++.h>
#include <cstdio>


#include "arb.h"
#include "arb_hypgeom.h"
#include "acb_hypgeom.h"
#include "acb_dirichlet.h"
#include "acb_modular.h"
#include "acb_calc.h"

#ifdef USE_MATPLOTLIB
#include "matplotlibcpp.h"
#endif


double to_double(const acb_t x);
double to_double(const arb_t x);
std::string to_string(const arb_t x, slong prec);


double diff(
    double (&f) (double),
    double x,
    double delta);


class MadecPlane{
  public:
    arb_t  fdeg, gdeg;
    arb_t  fdeg_j, gdeg_j;
    arb_t  yf, yg;
    arb_t  y_f, y_g;
    arb_t  yc_j;
    arb_t  yr;
    arb_t  yphi_integrand;
    acb_t  phi_exp_arg;
    slong  prec;

    arb_t  nj, njeq;
    arb_t  A0, A1, JB, B1, B0, JC, C1, C0, JE, E1, E0, D0, D1;

    MadecPlane(slong precision);
    void calc(const acb_t jc);
    void calc(const double jd);
    void print();
    void yplane(arb_t res, arb_t deg, slong prec);
    void yplane_deg(arb_t res, arb_t deg, slong prec);
    static void integrate_phi_exp_arg(acb_ptr res, acb_ptr j1, MadecPlane mp);
    static void integrate_phi_exp_arg(acb_ptr res, const double j, MadecPlane mp);
    static double calc_integral(const double j, MadecPlane mp);
};



namespace madec 
{
  using vector = std::vector<double>;

  const double ni = 180;
  const double nj = 150;
  const double njeq = 64;
  const double jspole = -7.450891;
  const double jnpole = 135.45089138; // f(j>jpole) > 0, thus system (1) in Madec1996 has no non-complex solutions
  const double jmin = 0.99*jspole;//-7.45;//-2.0;
  const double jmax = 0.99*jnpole;//135.45;//130.0;
  
  const double A0 = 1.04;
  const double A1 = -1.0*A0*njeq;
  
  const double JB = 14.0;
  const double B1 = 10.0;
  const double B0 = 0.52 * B1 / tanh(JB/B1);
  
  const double JC = 65.0;
  const double C1 = 8.0;
  const double C0 = 0.25 * C1 / tanh(JC/C1);
  
  const double JE = 65.0;
  const double E1 = 8.0;
  const double E0 =
      (0.5-1.0e-2)/(E1 * (2.0*tanh(JE/E1) - tanh((nj-njeq-JE)/E1)
      +    tanh((nj+njeq+JE)/E1 )));
  
  const double D0 = 0.5 - 2.0*E0*tanh(JE/E1)/E1;
  const double D1 = -1.0*D0*njeq;

  double fdeg(double j);
  double gdeg(double j);

  /*! Calculates 
      \f$
         \frac{\partial}{\partial j} \ln \frac
              { \cosh(a \cdot j-b-c)}
              {\cosh(a \cdot j-b+c)}
          = -2a \frac{\sinh(2c)}{(\cosh(2a \cdot j - 2b)+\cosh(2c))}
       \f$,
       with \f$ a=\frac{j}{L1}, b=\frac{j0}{L1}, c=\frac{JL}{L1}\f$,
       where symbol \f$ L \f$ in \f$ JL, L1 \f$ 
       stands for \f$ B, C, \text{or } D \f$.
  */ 
  double log_frac_cosh_j(double j, double j0, double JL, double L1);
  
  double fdeg_j(double j);
  double gdeg_j(double j);
  
  double fdeg_jj(double j);
  double gdeg_jj(double j);

  double deg2yplane(double deg);
  double yplane_deg(double deg);
  double yplane_deg2(double deg);

  double yc(double j);
  double yc_j(double j);
  double yc_jj(double j);
  
  double rad(double j);
  double rad_j(double j);


  //! Calculate \f$ \int_{njeq}^{j} -\frac{1}{R(j)} \frac{\partial yc}{\partial j} dj \f$
  double phi_exp_arg(double j, MadecPlane mp);

  double phi_plane(double phi0, double j, double phi_exp_argd, MadecPlane mp);
  double phi_plane(double phi0, double j, MadecPlane mp);

  double phi_i(double i, double phi_exp_argd, MadecPlane mp);
  double phi_j(double i, double j, double phi_exp_argd, MadecPlane mp);
  
  double i_to_phi0(double i);
  
  
  void ij_to_xy(double *x, double *y, double *iarr, double *jarr, int ni, int nj, MadecPlane mp);
  void ij_to_xy(
      double *x, double *x_i, double *x_j,
      double *y, double *y_i, double *y_j,
      double *iarr, double *jarr,
      int ni, int nj,
      MadecPlane mp);

  double mul(double a, double b);


  void get_lonlat(double *lon, double *lat, int &ni, int &nj);


  void ij_to_xy(
      double *x, double *x_i, double *x_j,
      double *y, double *y_i, double *y_j,
      double *iarr, double *jarr,
      int ni, int nj,
      MadecPlane mp);


  void ij_to_lonlat(
      double *lon, double *lon_i, double *lon_j,
      double *lat, double *lat_i, double *lat_j,
      double *iarr, double *jarr,
      int ni, int nj,
      MadecPlane mp);


  void generate_madec_grid(
      vector &iarr, vector &jarr,
      vector &lon, vector &lon_i, vector &lon_j,
      vector &lat, vector &lat_i, vector &lat_j,
      int &ni, int &nj);

  void write_grid(std::string &filename, int ni, int nj);

}

extern"C" void get_madec_grid_(
    double *iarr, double *jarr,
    double *lon, double *lon_i, double *lon_j,
    double *lat, double *lat_i, double *lat_j,
    double &jspole, double &jnpole, 
    const int &ni, const int &nj);
