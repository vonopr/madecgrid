#include "csv-out.h"
#include "geodesic.h"

#include <vector>


void geodesic::write_grid(std::string &filename, int ni, int nj)
{
  using vector = std::vector<double>;
  double pi = 4.0*std::atan(1.0);

  auto length{ni*nj};
  vector iarr(ni), jarr(nj);
  vector lon(length), lon_i(length), lon_j(length);
  vector lat(length), lat_i(length), lat_j(length);
  
  double const jspole = 0.0;
  double const jnpole = 1.0;
  
  double const lon_min{0.0};
  double const lon_max{2.0*pi};
  double const lat_min{-0.5*pi};
  double const lat_max{0.5*pi};
  
  double const xmin{0.0};
  double const xmax{1.0};
  double const ymin{0.0};
  double const ymax{1.0};
  
  for (int i=0; i<ni; i++){
    iarr[i] = double(i)/double(ni-1); // if i-axis is not cycled: i/nj -> i/(nj-1)
  }

  for (int j=0; j<nj; j++){
    jarr[j] = double(j)/double(nj-1);
  }
  
  for (int i=0; i<ni; i++){
    for (int j=0; j<ni; j++){
      int n = nj*i + j;
      lon[n] = lon_min + (lon_max-lon_min)*i/(ni-1); // if i-axis is not cycled: i/ni -> i/(ni-1)
      lat[n] = lat_min + (lat_max-lat_min)*j/(nj-1);
      
      lon_i[n] = (lon_max-lon_min)/(xmax-xmin);
      lon_j[n] = 0.0;

      lat_i[n] = 0.0;
      lat_j[n] = (lat_max-lat_min)/(ymax-ymin);
    }
  }


  std::vector<std::string> const headers{"i", "j", "I" , "J", "lon", "lat", "lon_I", "lat_I", "lon_J", "lat_J"};

  std::remove(filename.c_str());
  
  std::string delimiter(", ");
  writeCsvLine(filename, "#Grid size: " + std::to_string(ni) + delimiter + std::to_string(nj));
  writeCsvLine(filename, "#JPoles: " + to_string_full_precision(jspole) + delimiter + to_string_full_precision(jnpole));
  writeCsvLine(filename, "#Headers: " + join_strings(headers));
  for (int j=0; j<ni;j++)
    for (int i=0; i<ni;i++)
    {
      auto ind = nj*i + j;
      writeCsvLine(filename, std::make_tuple(i,j, iarr[i], jarr[j], lon[ind], lat[ind], lon_i[ind], lat_i[ind], lon_j[ind], lat_j[ind]));
    }
}

