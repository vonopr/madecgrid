#pragma once
#include <string>

namespace geodesic{
  void write_grid(std::string &filename, int ni, int nj);
}
