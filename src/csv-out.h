#pragma once

#include <iostream>
#include <mutex>
#include <fstream>
#include <limits>
#include <experimental/filesystem>
#include <cmath>
#include <iomanip>


//iterate over tuple in a prescribed order
template<class F, class...Ts, std::size_t...Is>
void for_each_in_tuple(const std::tuple<Ts...> & tuple, F func, std::index_sequence<Is...>)
{
    using expander = int[];
    (void)expander { 0, ((void)func(std::get<Is>(tuple)), 0)... };
}

//iterate over tuple
// auto  a = std::make_tuple(4, 5.1, "foo");
// for_each_in_tuple(a, [](const auto &x) { std::cout << x << ","; });
//
// Output:
//  4,5.1,foo
template<class F, class...Ts>
void for_each_in_tuple(const std::tuple<Ts...> & tuple, F func)
{
    for_each_in_tuple(tuple, func, std::make_index_sequence<sizeof...(Ts)>());
}

//iterate over tuple, skip last element
// auto  a = std::make_tuple(4, 5.1, "foo");
// for_each_in_tuple(a, [](const auto &x) { std::cout << x << ","; });
//
// Output:
//  4,5.1
template<class F, class...Ts>
void for_each_in_tuple_except_last(const std::tuple<Ts...> & tuple, F func)
{
    int constexpr tail_size = 1;
    for_each_in_tuple(tuple, func, std::make_index_sequence<sizeof...(Ts)-tail_size>());
}



template<typename T, class...Ts>
auto get_last_element(const std::tuple<Ts...> & tuple)
{
    int constexpr num_elems{static_cast<int>(std::tuple_size<T>{})};
    return std::get<num_elems-1>(tuple);
}



template <typename IntegralType>
int constexpr max_decimal_length_integral()
{
    static_assert(std::is_integral<IntegralType>::value, "Wrong type: must be integral");
    
    return static_cast<int>(std::numeric_limits<IntegralType>::digits10);
}



template <typename FloatingPointType>
int constexpr decimal_precision_save_accuracy()
{
    static_assert(std::is_floating_point<FloatingPointType>::value, "Wrong type: must be floating point");
    
    return static_cast<int>(std::numeric_limits<FloatingPointType>::max_digits10);
}



template <typename T>
int constexpr max_decimal_power()
{
    return std::log10(std::numeric_limits<T>::max());
}



template <typename T>
int constexpr max_decimal_power_length()
{
    //return 1 + static_cast<int>(std::log10(max_decimal_power<T>()));

    int constexpr length = 4; // std::log10 is not a constexpr yet; typical max of long double is 1.18973e+4932, four digits should be enough to store absolute value of decimal power
    return length;
}



template <typename FloatingPointType>
int constexpr decimal_width_save_accuracy()
{
    static_assert(std::is_floating_point<FloatingPointType>::value, "Wrong type: must be floating point");    

    int constexpr exp_value = max_decimal_power_length<FloatingPointType>();
    int constexpr exp_letter = 1;
    int constexpr exp_value_sign = 1;
    int constexpr value_sign = 1;
    int constexpr decimal_delimiter = 1;
    int constexpr extra_len = exp_value + exp_letter + exp_value_sign + value_sign + decimal_delimiter;

    return decimal_precision_save_accuracy<FloatingPointType>() + extra_len;
}



template <typename T>
std::string to_string_full_precision(T value)
{
    static_assert(std::is_floating_point<T>::value, "Wrong type: must be floating point");    

    int constexpr ndigits = decimal_precision_save_accuracy<T>();
    int constexpr length = decimal_width_save_accuracy<T>();
    int constexpr str_terminator_len = 1;

    char buffer[length + str_terminator_len];
    int num = sprintf(buffer, "%#*.*g", length, ndigits,  value);
    
    std::string value_str{buffer};
    return value_str;
}



template <typename T, typename Stream>
inline void print_value(T value, Stream &stream)
{
    if constexpr(std::is_floating_point<T>::value)
    {
        stream << to_string_full_precision(value);
    }
    else
    {
        stream << std::setw(max_decimal_length_integral<T>()) << value;
    }
}



template <typename T>
bool writeCsvLine(std::fstream &stream, T tuple) {
    if (stream) 
    {
        for_each_in_tuple_except_last(tuple, [&stream](auto const &x) {print_value(x, stream); stream << ",";});
        
        print_value(get_last_element<T>(tuple), stream);
        stream << std::endl;
        
        return true;
    } 
    else
    {
        return false;
    }
}



template <typename T>
bool writeCsvLine(std::string &fileName, T values) {
    std::fstream file;
    file.open(fileName, std::ios::out | std::ios::app);
    return writeCsvLine(file, values);
}


inline bool writeCsvLine(std::string &fileName, std::string string) {
    std::fstream stream;
    stream.open(fileName, std::ios::out | std::ios::app);

    if (stream) 
    { 
        stream << string;
        stream << std::endl;
        return true;
    } 
    else
    {
        return false;
    }
}

template <typename T=std::vector<std::string>>
std::string join_strings(T strings)
{
  std::string const delimeter{", "};
  std::string result{};
  
  for (auto const &string : strings)
  {
      result += (string + delimeter);
  }
  
  return result.substr(0, result.size() - delimeter.length());
}
