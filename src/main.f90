program madec
  integer:: ni, nj

  integer, parameter:: max_string_len = 100
  character(len=max_string_len):: arg_str
  character(len=max_string_len):: out_fname
  character(len=max_string_len):: out_format
  character(len=max_string_len):: units
  character(len=*), parameter:: nodescr = ""
  real:: rad_to_deg

  real, dimension(:), allocatable:: iarr, jarr
  real, dimension(:,:), allocatable:: lon, lon_i, lon_j
  real, dimension(:,:), allocatable:: lat, lat_i, lat_j
  real:: jspole, jnpole
  
  real, parameter:: i_greenwich = 45.0, i_180 = 135.0

!handle cmd-args
  arg_str = get_cmd_arg(num=1, default_value="724")
  read(arg_str,*) ni

  arg_str = get_cmd_arg(num=2, default_value="724")
  read(arg_str,*) nj

  out_fname = get_cmd_arg(num=3, default_value="madec_grid.bin")
  out_format = get_cmd_arg(num=4, default_value="binary")
  units = get_cmd_arg(num=5, default_value="radians")
  
  call check_format(out_format)
  call check_units(units)
  
  write(*,*) ni, nj
  write(*,*) trim(out_fname)
  write(*,*) trim(out_format)
  

!calculate longitudes, latitudes of the grid and their derivatives
  allocate(iarr(ni), jarr(nj))
  allocate(lon(ni,nj), lon_i(ni,nj), lon_j(ni,nj))
  allocate(lat(ni,nj), lat_i(ni,nj), lat_j(ni,nj))

  call madec_lonlat(&
           iarr, jarr,&
           lon, lon_i, lon_j,&
           lat, lat_i, lat_j,&
           jspole, jnpole,&
           ni, nj)
  

!write grid to file
  call write_header(out_fname, ni, nj, iarr(1), iarr(ni), jarr(1), jarr(nj), jspole, jnpole, "grid parameters", out_format)
  
  !call write_arr1d(out_fname, iarr, ni, "iarr", units, out_format)
  !call write_arr1d(out_fname, jarr, nj, "jarr", units, out_format)

  call write_arr2d(out_fname, lon, ni, nj, "longitude", units, out_format)
  call write_arr2d(out_fname, lon_i, ni, nj, "lon_i", units, out_format)
  call write_arr2d(out_fname, lon_j, ni, nj, "lon_j", units, out_format)

  call write_arr2d(out_fname, lat, ni, nj, "latitude", units, out_format)
  call write_arr2d(out_fname, lat_i, ni, nj, "lat_i", units, out_format)
  call write_arr2d(out_fname, lat_j, ni, nj, "lat_j", units, out_format)


contains

  function get_cmd_arg(num, default_value)
    integer, intent(in):: num
    character(len=*), intent(in):: default_value
    character(max_string_len):: get_cmd_arg
    call get_command_argument(number=num, value=get_cmd_arg)
    if(get_cmd_arg.eq."") then
      get_cmd_arg = default_value
    end if
  end function


  subroutine check_format(format_str)
    character(len=*), intent(in):: format_str
    if(.not.(format_str.eq."table".or.format_str.eq."binary")) then
      write(*,"(a)") "ERROR: incompatible output format was set: "//trim(format_str)//"."
      write(*,"(a)") "       Accepted values are: `binary`, `table`."
      write(*,"(a)") "       Check command-line-arguments."
      stop 1
    end if
  end subroutine


  subroutine check_units(units)
    character(len=*), intent(in):: units
    if(.not.(units.eq."degrees".or.units.eq."radians")) then
      write(*,"(a)") "ERROR: incompatible units of output values were set: "//trim(units)//"."
      write(*,"(a)") "       Accepted values are: `degrees`, `radians`."
      write(*,"(a)") "       Check command-line-arguments."
      stop 2
    end if
  end subroutine


  function get_units_factor(units)
    character(len=*), intent(in):: units
    
    if(units.eq."degrees") then
      get_units_factor = 180.0/(4.0*atan(1.0))
    elseif(units.eq."") then
      get_units_factor = 1.0
    else
      get_units_factor = 1.0
    end if
  end function
    

  subroutine write_header(fname, ni, nj, i1, i2, j1, j2, jspole, jnpole, description, format_str)
    character(len=*), intent(in):: fname
    integer, intent(in):: ni, nj
    real, intent(in):: i1, i2, j1, j2
    real, intent(in):: jspole, jnpole
    character(len=*), intent(in):: description
    character(len=*), intent(in):: format_str
    
    if(format_str.eq."binary") then
      call write_header_binary(fname, ni, nj, i1, i2, j1, j2, jspole, jnpole, description)
    else
      call write_header_ascii(fname, ni, nj, i1, i2, j1, j2, jspole, jnpole, description)
    end if
  end subroutine


  subroutine write_header_binary(fname, ni, nj, i1, i2, j1, j2, jspole, jnpole, description)
    character(len=*), intent(in):: fname
    integer, intent(in):: ni, nj
    real, intent(in):: i1, i2, j1, j2
    real, intent(in):: jspole, jnpole
    character(len=*), intent(in):: description
    
    integer:: funit
    
    open(newunit=funit, file=out_fname, position="rewind", form="unformatted", access="stream")
    write(funit) ni, nj
    write(funit) i1
    write(funit) i2
    write(funit) j1
    write(funit) j2
    write(funit) i_greenwich, i_180
    write(funit) jspole, jnpole
    close(funit)
  end subroutine


  subroutine write_header_ascii(fname, ni, nj, i1, i2, j1, j2, jspole, jnpole , description)
    character(len=*), intent(in):: fname
    integer, intent(in):: ni, nj
    real, intent(in):: i1, i2, j1, j2
    real, intent(in):: jspole, jnpole
    character(len=*), intent(in):: description
    
    integer:: funit
    
    open(newunit=funit, file=out_fname, position="rewind")
    write(funit,"(a)") "#"//description//":"
    write(funit,*) ni, nj
    write(funit,*) i1, i2, j1, j2
    write(funit,*) i_greenwich, i_180
    write(funit,*) jspole, jnpole
    close(funit)
  end subroutine


  subroutine write_arr1d(fname, array, nelems, description, units, format_str)
    character(len=*), intent(in):: fname
    integer, intent(in):: nelems
    real, dimension(nelems), intent(in):: array
    character(len=*), intent(in):: description
    character(len=*), intent(in):: units
    character(len=*), intent(in):: format_str
    
    real:: factor

    factor = get_units_factor(units)

    if(format_str.eq."binary") then
      call write_arr1d_binary(fname, array, nelems, description, factor)
    else
      call write_arr1d_ascii(fname, array, nelems, description, factor)
    end if

  end subroutine


  subroutine write_arr2d(fname, array, ni, nj, description, units, format_str)
    character(len=*), intent(in):: fname
    integer, intent(in):: ni, nj
    real, dimension(ni,nj), intent(in):: array
    character(len=*), intent(in):: description
    character(len=*), intent(in):: units
    character(len=*), intent(in):: format_str
    
    real:: factor

    factor = get_units_factor(units)

    if(format_str.eq."binary") then
      call write_arr2d_binary(fname, array, ni, nj, description, factor)
    else
      call write_arr2d_ascii(fname, array, ni, nj, description, factor)
    end if

  end subroutine


  subroutine write_arr1d_binary(fname, array, nelems, description, factor)
    character(len=*), intent(in):: fname
    integer, intent(in):: nelems
    real, dimension(nelems), intent(in):: array
    character(len=*), intent(in):: description
    real, intent(in):: factor

    integer:: funit

    open(newunit=funit, file=out_fname, position="append", form="unformatted", access="stream")
    write(funit) factor*array
    close(funit)
  end subroutine


  subroutine write_arr2d_binary(fname, array, ni, nj, description, factor)
    character(len=*), intent(in):: fname
    integer, intent(in):: ni, nj
    real, dimension(ni,nj), intent(in):: array
    character(len=*), intent(in):: description
    real, intent(in):: factor

    integer:: funit

    open(newunit=funit, file=out_fname, position="append", form="unformatted", access="stream")
    write(funit) factor*array
    close(funit)
  end subroutine


  subroutine write_arr1d_ascii(fname, array, nelems, description, factor)
    character(len=*), intent(in):: fname
    integer, intent(in):: nelems
    real, dimension(nelems), intent(in):: array
    character(len=*), intent(in):: description
    real, intent(in):: factor
    
    integer:: num
    integer:: funit

    open(newunit=funit, file=out_fname, position="append")
    write(funit,"(a)") "#"//description//":"
  
    do num=1, nelems
      write(funit,"(f12.5)", advance="no") factor*array(num)
    end do
    write(funit,*)
    close(funit)
  
  end subroutine


  subroutine write_arr2d_ascii(fname, array, ni, nj, description, factor)
    character(len=*), intent(in):: fname
    integer, intent(in):: ni, nj
    real, dimension(ni,nj), intent(in):: array
    character(len=*), intent(in):: description
    real, intent(in):: factor
    
    integer:: i,j
    integer:: funit

    open(newunit=funit, file=out_fname, position="append")
    write(funit,"(a)") "#"//description//":"
  
    do j=1, nj
      do i=1, ni
        write(funit,"(f12.5)", advance="no") factor*array(i,j)
      end do
      write(funit,*)
    end do
    close(funit)
  
  end subroutine



  
end program
