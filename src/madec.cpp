#include "madec.h"
#include "csv-out.h"

double to_double(const acb_t x){
  const int buf_size = 256;
  return atof(arb_get_str(acb_realref(x), buf_size, ARB_STR_NO_RADIUS|ARB_STR_MORE));
}

double to_double(const arb_t x){
  const int buf_size = 256;
  return atof(arb_get_str(x, buf_size, ARB_STR_NO_RADIUS|ARB_STR_MORE));
}

std::string to_string(const arb_t x, slong prec){
  return std::string(arb_get_str(x, prec, ARB_STR_NO_RADIUS|ARB_STR_MORE));
}

double diff(
    double (&f) (double),
    double x,
    double delta){
    
  return (f(x+0.5*delta)-f(x-0.5*delta))/delta;
}

namespace madec 
{
  double fdeg(double j){
    double res =
      A0*j
      +    A1
      +    B0 * log(cosh((j-njeq-JB)/(B1)) / cosh((j-njeq+JB)/(B1))) 
      -    C0 * log(cosh((j-njeq-JC)/(C1)) / cosh((j-njeq+JC)/(C1)));
    return res;
  }
  
  double gdeg(double j){
    double res;
  
    if (j>njeq){
      res = D0*j + D1 - E0*log(cosh((j-njeq-JE)/(E1)) / cosh((j-njeq+JE)/E1));
    }else{
      res = fdeg(j);
    };
  
    return res;
  }
  

  double log_frac_cosh_j(double j, double j0, double JL, double L1){
    double a = 1/L1;
    double b = j0/L1;
    double c = JL/L1;
    
    return -2.0*a*sinh(2*c)/(cosh(2*a*j-2*b)+cosh(2*c));
  }

  double log_frac_cosh_jj(double j, double j0, double JL, double L1){
    double ap = 1/L1*(j-j0+JL);
    double am = 1/L1*(j-j0-JL);
    
    double chp = cosh(ap);
    double chm = cosh(am);
    double chfrac = chm/chp;

    return (1.0-chfrac*chfrac)/(L1*L1*chm*chm);
  }


  double fdeg_j(double j){
    double B1_expr = B1*cosh((j-njeq+JB)/B1)*cosh((j-njeq+JB)/B1);
    double C1_expr = C1*cosh((j-njeq+JC)/C1)*cosh((j-njeq+JC)/C1);
  
    double res =
      A0
      +   B0*log_frac_cosh_j(j, njeq, JB, B1)
      -   C0*log_frac_cosh_j(j, njeq, JC, C1);
    return res;
  }

  double fdeg_jj(double j){
    double res = B0*log_frac_cosh_jj(j, njeq, JB, B1) - C0*log_frac_cosh_jj(j, njeq, JC, C1);
    return res;
  }
  
  double gdeg_j(double j){
    double res;
  
    if (j > njeq){
      res = D0 - E0*log_frac_cosh_j(j, njeq, JE, E1);
    }else{
      res = fdeg_j(j);
    }
  
    return res;
  }

  double gdeg_jj(double j){
    double res;
  
    if (j > njeq){
      res = - E0*log_frac_cosh_jj(j, njeq, JE, E1);
    }else{
      res = fdeg_jj(j);
    }
    return res;
  }

  double deg2yplane(double deg){
    const double deg360inv = 1.0/360.0;
    const double d4inv = 1.0/4.0;
    
    double pi = 4.0*std::atan(1.0);
    double res = tan(d4inv*pi - pi*deg360inv*deg);

    return res;
  }
  
  double yplane_deg(double deg){
    const double deg360inv = 1.0/360.0;
    const double d4inv = 1.0/4.0;
    
    double pi = 4.0*std::atan(1.0);
    double arg = d4inv*pi + pi*deg360inv*deg;
    double sin_arg = sin(arg);
    double res = -pi*deg360inv/(sin_arg*sin_arg);
    return res;
  }

  double yplane_deg2(double deg){   
    double pi = 4.0*std::atan(1.0);
    double pi_360 = pi/360.0;
    double arg = pi/4.0 + pi/360.0*deg;
    double sn = sin(arg);
    double cs = cos(arg);
    double res = 2.0*pi_360*pi_360*cs/(sn*sn*sn);
    return res;
  }


  double yc(double j){
    return 0.5*( deg2yplane(gdeg(j)) - deg2yplane(fdeg(j)));
  }

  double yc_j(double j){
    const double d2inv = 1.0/2.0;
    double res = d2inv*(yplane_deg(gdeg(j))*gdeg_j(j) - yplane_deg(fdeg(j))*fdeg_j(j));
    return res;
  }

  double yc_jj(double j){
    const double d2inv = 1.0/2.0;
    double res = d2inv*
      (
        +  yplane_deg2(gdeg(j))*gdeg_j(j)*gdeg_j(j)
        -  yplane_deg2(fdeg(j))*fdeg_j(j)*fdeg_j(j)
        +  yplane_deg(gdeg(j))*gdeg_jj(j)
        -  yplane_deg(fdeg(j))*fdeg_jj(j)
      );
    return res;
  }

  double rad(double j){
    double res = sqrt(deg2yplane(fdeg(j))*deg2yplane(gdeg(j)));
    return res;
  }

  double rad_j(double j){
    double yf = deg2yplane(fdeg(j));
    double yg = deg2yplane(gdeg(j));
    double rad_inv = 1.0/sqrt(yf*yg);
    
    double res = 0.5*rad_inv*(yg*yplane_deg(fdeg(j))*fdeg_j(j) + yf*yplane_deg(gdeg(j))*gdeg_j(j));
    return res;
  }
  
  //double phi_j(double j, double phi0){
    //double pi = 4.0*std::atan(1.0);

    //double res = 
      //2.0*(-pi/4.0 + atan(tan(phi0 + pi/4.0)*exp(-yc_j(j)/rad(j))));
    //return res;
  //};



  double phi_exp_arg(double j, MadecPlane mp){
    void* param = &mp;
    double res;
  
    if (j > madec::njeq){
      res = MadecPlane::calc_integral(j, mp);
    }else{
      res = 0.0;
    }
    return res;
  };


  double phi_plane(double phi0, double j, double phi_exp_argd, MadecPlane mp){
    double pi = 4.0*std::atan(1.0);
    double res = -0.5*pi +2.0*atan(tan(0.25*pi+0.5*phi0)*exp(phi_exp_argd));
    return res;
  };
  
  double phi_plane(double phi0, double j, MadecPlane mp){
    double res = phi_plane(phi0, j, phi_exp_arg(j, mp), mp);
    return res;

  };


  double phi0_i(){
    double pi = 4.0*std::atan(1.0);
    return -1.0*(2.0*pi)/double(madec::ni);
  }

  double i_to_phi0(double i){
    return phi0_i()*i;
  }

  double phi_j(double i, double j, double phi_exp_argd, MadecPlane mp){
    double phi0 = i_to_phi0(i);
    return  (-yc_j(j)/rad(j))*cos(phi0)/(sin(phi0)*sinh(phi_exp_argd) + cosh(phi_exp_argd));
  };


  double phi_i(double i, double phi_exp_argd, MadecPlane mp){
    double phi0 = i_to_phi0(i);
    double pi = 4.0*std::atan(1.0);
    double tang = tan(0.25*pi+0.5*phi0);
    double exp_arg = exp(phi_exp_argd);
    
    //return (1.0 + tang*tang)*exp_arg  /  (1.0 + tang*tang*exp_arg*exp_arg) * phi0_i();
    return phi0_i()/(cosh(phi_exp_argd) + sinh(phi_exp_argd)*sin(phi0));
    
  }


  
  double ij_to_phi(double i, double j, MadecPlane mp){
    double phi0 = i_to_phi0(i);
    return phi_plane(phi0, j, mp);
  }
  
  void phij_to_xy(double phi, double j, double x, double y, MadecPlane mp){
    x = madec::rad(j) * cos(phi);
    y = madec::yc(j) + madec::rad(j) * sin(phi);
  
  }


  void ij_to_xy(
      double *x, double *x_i, double *x_j,
      double *y, double *y_i, double *y_j,
      double *iarr, double *jarr,
      int ni, int nj,
      MadecPlane mp)
  {
    namespace ph = std::placeholders;
  
    double *phi0;
    phi0 = new double[ni];
  
    for (int i_count=0;i_count<ni;i_count++){

      double i = iarr[i_count];

      for (int j_count=0;j_count<nj;j_count++){
        double j = jarr[j_count];
        int n = nj*i_count + j_count;
        
        double phi_exp_argd = phi_exp_arg(j, mp);
        double phi0 = i_to_phi0(i);
        double phi = phi_plane(phi0, j, phi_exp_argd, mp);
        double phi_id = phi_i(i, phi_exp_argd, mp);
        double phi_jd = phi_j(i, j, phi_exp_argd, mp);
        
        double radd = madec::rad(j);
        double rad_jd = madec::rad_j(j);
        
        double ycd = madec::yc(j);
        double yc_jd = madec::yc_j(j);
        
        double cs = cos(phi);
        double sn = sin(phi);

        x[n] = radd*cs;
        x_i[n] = -radd*sn*phi_id;
        x_j[n] = rad_jd*cs - radd*sn*phi_jd;

        y[n] = ycd + radd*sn;
        y_i[n] = radd*cs*phi_id;
        y_j[n] = yc_jd + rad_jd*sn + radd*cs*phi_jd;

      }
    }
  }



  void ij_to_xy(double *x, double *y, double *iarr, double *jarr, int ni, int nj, MadecPlane mp){
    namespace ph = std::placeholders;
  
    double *phi0;
    phi0 = new double[ni];
    std::transform(iarr, iarr+ni, phi0, i_to_phi0);
  
    for (int i=0;i<ni;i++){
      for (int j=0;j<nj;j++){
        double phi = ij_to_phi(iarr[i], jarr[j], mp);
        x[nj*i + j] = madec::rad(j) * cos(phi);
        y[nj*i + j] = madec::yc(j) + madec::rad(j) * sin(phi);
  
      }
    }
  }
  
  void xy_to_lonlat(double *lon, double *lat, double *x, double *y, int ni, int nj){
    double pi = 4.0*std::atan(1.0);
    double rad_to_deg = 180.0/pi;
  
    for (int i=0;i<ni;i++){
      for (int j=0;j<nj;j++){
  
        lon[nj*i + j] = rad_to_deg * atan2(x[nj*i + j], y[nj*i + j]);
        lat[nj*i + j] = rad_to_deg * (0.5*pi-2.0*atan(x[nj*i + j]*x[nj*i + j] + y[nj*i + j]*y[nj*i + j]));
  
      }
    }
  }


  void ij_to_lonlat(
      double *lon, double *lon_i, double *lon_j,
      double *lat, double *lat_i, double *lat_j,
      double *iarr, double *jarr,
      int ni, int nj,
      MadecPlane mp)
  {  
    double *x, *x_i, *x_j;
    double *y, *y_i, *y_j;
    
    x = new double[ni*nj];
    x_i = new double[ni*nj];
    x_j = new double[ni*nj];
  
    y = new double[ni*nj];
    y_i = new double[ni*nj];
    y_j = new double[ni*nj];

    ij_to_xy(x, x_i, x_j, y, y_i, y_j, iarr, jarr, ni, nj, mp);
    
    double pi = 4.0*std::atan(1.0);
  
    for (int i=0;i<ni;i++){
      for (int j=0;j<nj;j++){
        int n = nj*i + j;
        double rad2 = x[n]*x[n] + y[n]*y[n];
        double rad2inv = 1.0/rad2;
        double rad = sqrt(rad2);
  
        lon[n] = atan2(x[n], y[n]);
        lon_i[n] = (x_i[n]*y[n] - y_i[n]*x[n])*rad2inv;
        lon_j[n] = (x_j[n]*y[n] - y_j[n]*x[n])*rad2inv;

        
        lat[n] = (0.5*pi-2.0*atan(rad));
        lat_i[n] = -2.0 * (x[n]*x_i[n] + y[n]*y_i[n]) / ((1.0+rad2)*rad);
        lat_j[n] = -2.0 * (x[n]*x_j[n] + y[n]*y_j[n]) / ((1.0+rad2)*rad);
      }
    }

  }


  
  void ij_to_lonlat(double *lon, double *lat, double *iarr, double *jarr, int ni, int nj, MadecPlane mp){  
    double *x;
    double *y;
    
    x = new double[ni*nj];
    y = new double[ni*nj];
    
    ij_to_xy(x, y, iarr, jarr, ni, nj, mp);
    xy_to_lonlat(lon, lat, x, y,  ni, nj);
    
  };
  
  
  double mul(double a, double b){
    return a*b;
  }

  void set_ij(double *iarr, double *jarr, int ni, int nj, MadecPlane mp){
    namespace ph = std::placeholders;
  
    double di = madec::ni/double(ni);
    double dj = (madec::jmax - madec::jmin)/double(nj);
    
    std::iota(iarr, iarr + ni, 0.0);
    std::iota(jarr, jarr + nj, 0.0);

    for(int i=0;i<ni;i++){
      iarr[i] = di*i;
    }


    int jeq = nj/2.0;

    for(int j=0;j<nj;j++){
      jarr[j] = madec::njeq + dj*(j-jeq);
    }

  }
  
  void get_lonlat(double *lon, double *lat, int &ni, int &nj){
    const slong  prec = 40;
    MadecPlane mp(prec);
  
    double iarr[ni];
    double jarr[nj];
    
    set_ij(iarr, jarr, ni, nj, mp);
    ij_to_lonlat(lon, lat, iarr, jarr,  ni, nj, mp);
    
  };

  void get_grid(
      double *iarr, double *jarr,
      double *lon, double *lon_i, double *lon_j,
      double *lat, double *lat_i, double *lat_j,
      int &ni, int &nj)
  {
    const slong  prec = 40;
    MadecPlane mp(prec);
  
    //double iarr[ni];
    //double jarr[nj];
    set_ij(iarr, jarr, ni, nj, mp);
    ij_to_lonlat(
        lon, lon_i, lon_j,
        lat, lat_i, lat_j, 
        iarr, jarr,
        ni, nj,
        mp);
    
  };


  void generate_madec_grid(
      vector &iarr, vector &jarr,
      vector &lon, vector &lon_i, vector &lon_j,
      vector &lat, vector &lat_i, vector &lat_j,
      int &ni, int &nj)
  {

    madec::get_grid(
      &iarr.front(), &jarr.front(),
      &lon.front(), &lon_i.front(), &lon_j.front(),
      &lat.front(), &lat_i.front(), &lat_j.front(),
      ni, nj);
  };


  struct Table
  {
    enum width{index_width = 3, madec_ij_width = 13, lonlat_width = 13, lonlat_ij_width = 12};
  };

  std::vector<std::string> const headers{"i", "j", "I" , "J", "lon", "lat", "lon_I", "lat_I", "lon_J", "lat_J"};

  template<typename Table=Table>
  void print_table_headers()
  {
    std::cout << std::setw(Table::index_width) << "i";
    std::cout << std::setw(Table::index_width+2) << "j";
    std::cout << std::setw(Table::madec_ij_width+2) << "I" ;
    std::cout << std::setw(Table::madec_ij_width+2) << "J" ;
    std::cout << std::setw(Table::lonlat_width+2) << "lon" ;
    std::cout << std::setw(Table::lonlat_width+2) << "lat" ;
    std::cout << std::setw(Table::lonlat_ij_width+2) << "lon_I" ;
    std::cout << std::setw(Table::lonlat_ij_width+2) << "lat_I" ;
    std::cout << std::setw(Table::lonlat_ij_width+2) << "lon_J" ;
    std::cout << std::setw(Table::lonlat_ij_width+2) << "lat_J" ;
    std::cout << std::endl;
  }


  template<typename T>
  void print_table_element(T value, int column_width)
  {
      std::cout.width(column_width);
      std::cout << std::fixed;
      std::cout << value;
      std::cout << " |";
  }


  template<typename Table=Table>
  void print_table_line(
    int const &i, int const &j,
    double const &imadec, double const &jmadec,
    double const &lon, double const &lat,
    double const &lon_i, double const &lat_i,
    double const &lon_j, double const &lat_j)
  {

    print_table_element(i, Table::index_width);
    print_table_element(j, Table::index_width);

    print_table_element(imadec, Table::madec_ij_width);
    print_table_element(jmadec, Table::madec_ij_width);

    double pi = 4.0*std::atan(1.0);
    print_table_element(lon*180.0d/pi, Table::lonlat_width);
    print_table_element(lat*180.0d/pi, Table::lonlat_width);

    print_table_element(lon_i*180.0d/pi, Table::lonlat_ij_width);
    print_table_element(lat_i*180.0d/pi, Table::lonlat_ij_width);

    print_table_element(lon_j*180.0d/pi, Table::lonlat_ij_width);
    print_table_element(lat_j*180.0d/pi, Table::lonlat_ij_width);

    std::cout << std::endl;
  }; 
  
  void write_grid(std::string &filename, int ni, int nj)
  {
    auto length{ni*nj};
    vector iarr(ni), jarr(nj);
    vector lon(length), lon_i(length), lon_j(length);
    vector lat(length), lat_i(length), lat_j(length);
    
    madec::generate_madec_grid(
      iarr, jarr,
      lon, lon_i, lon_j,
      lat, lat_i, lat_j,
      ni, nj);


    //print_table_headers();
    for (int j=0; j<ni;j++)
      for (int i=0; i<ni;i++)
      {
        auto ind = nj*i + j;
        //print_table_line(i,j, iarr[i], jarr[j], lon[ind], lat[ind], lon_i[ind], lat_i[ind], lon_j[ind], lat_j[ind]);
      }

    std::remove(filename.c_str());
    
    std::string delimiter(", ");
    writeCsvLine(filename, "#Grid size: " + std::to_string(ni) + delimiter + std::to_string(nj));
    writeCsvLine(filename, "#JPoles: " + to_string_full_precision(jspole) + delimiter + to_string_full_precision(jnpole));
    writeCsvLine(filename, "#Headers: " + join_strings(headers));
    for (int j=0; j<ni;j++)
      for (int i=0; i<ni;i++)
      {
        auto ind = nj*i + j;
        writeCsvLine(filename, std::make_tuple(i,j, iarr[i], jarr[j], lon[ind], lat[ind], lon_i[ind], lat_i[ind], lon_j[ind], lat_j[ind]));
      }
  }

}


  extern"C" void get_madec_grid_(
      double *iarr, double *jarr,
      double *lon, double *lon_i, double *lon_j,
      double *lat, double *lat_i, double *lat_j,
      double &jspole, double  &jnpole,
      const int &ni, const int &nj)
  {
    int ni_convert = ni;
    int nj_convert = nj;
    madec::get_grid(
      iarr, jarr,
      lon, lon_i, lon_j,
      lat, lat_i, lat_j,
      ni_convert, nj_convert);
    jspole = madec::jspole;
    jnpole = madec::jnpole;
  };




MadecPlane::MadecPlane(slong precision){
  prec = precision;

  arb_init(nj);
  arb_init(njeq);
  arb_init(A0);
  arb_init(A1);
  arb_init(JB);
  arb_init(B1);
  arb_init(B0);
  arb_init(JC);
  arb_init(C1);
  arb_init(C0);
  arb_init(JE);
  arb_init(E1);
  arb_init(E0);
  arb_init(D0);
  arb_init(D1);


  
  arb_set_d(nj, madec::nj);
  arb_set_d(njeq, madec::njeq);


  arb_set_str(A0, "1.04", prec);    
  arb_mul(A1, A0, njeq, prec);
  arb_neg(A1, A1);


  arb_set_d(JB, madec::JB);
  arb_set_d(B1, madec::B1);
  arb_t  B00;
  arb_init(B00);
  arb_set_str(B00, "0.52", prec);
  arb_div(B0, JB,  B1, prec);
  arb_tanh(B0, B0, prec);
  arb_div(B0, B1,  B0, prec);
  arb_mul(B0, B00,  B0, prec);


  arb_set_d(JC, madec::JC);
  arb_set_d(C1, madec::C1);

  arb_t  C00;
  arb_init(C00);
  arb_set_str(C00, "0.25", prec);

  arb_div(C0, JC,  C1, prec);
  arb_tanh(C0, C0, prec);
  arb_div(C0, C1,  C0, prec);
  arb_mul(C0, C00,  C0, prec);


  arb_set_d(JE, madec::JE);
  arb_set_d(E1, madec::E1);
  arb_t  E00;

  arb_t  tanhE1, tanhE2, tanhE3;

  arb_init(E00);
  arb_init(tanhE1);
  arb_init(tanhE2);
  arb_init(tanhE3);

  arb_set_str(E00, "0.49", prec);

  const int two_int = 2;
  arb_t  two;
  arb_init(two);
  arb_set_si(two, two_int);
  arb_div(tanhE1, JE,  E1, prec);
  arb_tanh(tanhE1, tanhE1, prec);
  arb_mul(tanhE1, two, tanhE1, prec);

  arb_sub(tanhE2, nj, njeq, prec);
  arb_sub(tanhE2, tanhE2, JE, prec);
  arb_div(tanhE2, tanhE2, E1, prec);
  arb_tanh(tanhE2, tanhE2, prec);
  
  arb_add(tanhE3, nj, njeq, prec);
  arb_add(tanhE3, tanhE3, JE, prec);
  arb_div(tanhE3, tanhE3, E1, prec);
  arb_tanh(tanhE3, tanhE3, prec);
  
  arb_t  divisor;
  arb_init(divisor);
  arb_sub(divisor, tanhE1, tanhE2, prec);
  arb_add(divisor, divisor, tanhE3, prec);
  arb_mul(divisor, E1, divisor, prec);
  
  arb_div(E0, E00, divisor, prec);
  
  
 //   const double E0 =
 //     (0.5-1.0e-2)/(E1 * (2.0*tanh(JE/E1) - tanh((nj-njeq-JE)/E1)
 //    +    tanh((nj+njeq+JE)/E1 )));


  arb_t  half;
  arb_init(half);
  arb_inv(half, two, prec);

  arb_t  tanhD0;
  arb_init(tanhD0);
  arb_div(tanhD0, JE,  E1, prec);
  arb_tanh(tanhD0, tanhD0, prec);
  
  arb_div(D0, tanhD0, E1, prec);
  arb_mul(D0, D0, E0, prec);
  arb_mul(D0, two, D0, prec);
  arb_neg(D0, D0);
  arb_add(D0, D0, half, prec);
  
  arb_mul(D1, D0, njeq, prec);
  arb_neg(D1, D1);


  arb_init(fdeg);
  arb_init(gdeg);
  arb_init(fdeg_j);
  arb_init(gdeg_j);
  arb_init(yf);
  arb_init(yg);
  arb_init(y_f);
  arb_init(y_g);
  arb_init(yc_j);
  arb_init(yr);
  arb_init(yphi_integrand);
  acb_init(phi_exp_arg);
};



void MadecPlane::calc(const acb_t jc){
  arb_t coshB1m, coshB1p;
  arb_t coshC1m, coshC1p;
  arb_t coshE1m, coshE1p;
  arb_t B0_fdeg, C0_fdeg, E0_gdeg;
  arb_t dj;
  
  arb_init(coshB1m);
  arb_init(coshB1p);
  arb_init(coshC1m);
  arb_init(coshC1p);
  arb_init(coshE1m);
  arb_init(coshE1p);
  arb_init(B0_fdeg);
  arb_init(C0_fdeg);
  arb_init(E0_gdeg);
  arb_init(dj);


  arb_sub(dj, acb_realref(jc), njeq, prec);

  //fdeg(j)=
  //  A0*j
  //  +    A1
  //  +    B0 * log(cosh((j-njeq-JB)/(B1)) / cosh((j-njeq+JB)/(B1))) 
  //  -    C0 * log(cosh((j-njeq-JC)/(C1)) / cosh((j-njeq+JC)/(C1)));

  arb_sub(coshB1m, dj, JB, prec);
  arb_div(coshB1m, coshB1m, B1, prec);
  arb_cosh(coshB1m, coshB1m, prec);

  arb_add(coshB1p, dj, JB, prec);
  arb_div(coshB1p, coshB1p, B1, prec);
  arb_cosh(coshB1p, coshB1p, prec);

  arb_div(B0_fdeg, coshB1m, coshB1p, prec);
  arb_log(B0_fdeg, B0_fdeg, prec);
  arb_mul(B0_fdeg, B0, B0_fdeg, prec);


  arb_sub(coshC1m, dj, JC, prec);
  arb_div(coshC1m, coshC1m, C1, prec);
  arb_cosh(coshC1m, coshC1m, prec);

  arb_add(coshC1p, dj, JC, prec);
  arb_div(coshC1p, coshC1p, C1, prec);
  arb_cosh(coshC1p, coshC1p, prec);

  arb_div(C0_fdeg, coshC1m, coshC1p, prec);
  arb_log(C0_fdeg, C0_fdeg, prec);
  arb_mul(C0_fdeg, C0, C0_fdeg, prec);


  arb_mul(fdeg, A0, acb_realref(jc), prec);
  arb_add(fdeg, fdeg, A1, prec);
  arb_add(fdeg, fdeg, B0_fdeg, prec);
  arb_sub(fdeg, fdeg, C0_fdeg, prec);



  //gdeg(j)  = D0*j + D1 - E0*log(cosh((j-njeq-JE)/(E1)) / cosh((j-njeq+JE)/E1));
  arb_sub(coshE1m, dj, JE, prec);
  arb_div(coshE1m, coshE1m, E1, prec);
  arb_cosh(coshE1m, coshE1m, prec);

  arb_add(coshE1p, dj, JE, prec);
  arb_div(coshE1p, coshE1p, E1, prec);
  arb_cosh(coshE1p, coshE1p, prec);

  arb_div(E0_gdeg, coshE1m, coshE1p, prec);
  arb_log(E0_gdeg, E0_gdeg, prec);
  arb_mul(E0_gdeg, E0, E0_gdeg, prec);

  arb_mul(gdeg, D0, acb_realref(jc), prec);
  arb_add(gdeg, gdeg, D1, prec);
  arb_sub(gdeg, gdeg, E0_gdeg, prec);

// fdeg_j(j) = 
//    A0
//    +    B0/B1*sinh((j-njeq-JB)/B1)/(cosh((j-njeq-JB)/B1))  //fdeg_j1
//    -    B0/B1*sinh((j-njeq+JB)/B1)/(cosh((j-njeq+JB)/B1))  //fdeg_j2
//    -    C0/C1*sinh((j-njeq-JC)/C1)/(cosh((j-njeq-JC)/C1))  //fdeg_j3
//    +    C0/C1*sinh((j-njeq+JC)/C1)/(cosh((j-njeq+JC)/C1))  //fdeg_j4
  
  arb_t sinhB1m, sinhB1p;
  arb_t sinhC1m, sinhC1p;
  arb_t sinhE1m, sinhE1p;
  
  arb_init(sinhB1m);
  arb_init(sinhB1p);
  arb_init(sinhC1m);
  arb_init(sinhC1p);
  arb_init(sinhE1m);
  arb_init(sinhE1p);

  arb_sub(sinhB1m, dj, JB, prec);
  arb_div(sinhB1m, sinhB1m, B1, prec);
  arb_sinh(sinhB1m, sinhB1m, prec);

  arb_add(sinhB1p, dj, JB, prec);
  arb_div(sinhB1p, sinhB1p, B1, prec);
  arb_sinh(sinhB1p, sinhB1p, prec);

  arb_sub(sinhC1m, dj, JC, prec);
  arb_div(sinhC1m, sinhC1m, C1, prec);
  arb_sinh(sinhC1m, sinhC1m, prec);

  arb_add(sinhC1p, dj, JC, prec);
  arb_div(sinhC1p, sinhC1p, C1, prec);
  arb_cosh(sinhC1p, sinhC1p, prec);

  arb_sub(sinhE1m, dj, JE, prec);
  arb_div(sinhE1m, sinhE1m, E1, prec);
  arb_sinh(sinhE1m, sinhE1m, prec);

  arb_add(sinhE1p, dj, JE, prec);
  arb_div(sinhE1p, sinhE1p, E1, prec);
  arb_cosh(sinhE1p, sinhE1p, prec);

  
  arb_t fdeg_jB, fdeg_jC, gdeg_jE;
  arb_init(fdeg_jB);
  arb_init(fdeg_jC);
  arb_init(gdeg_jE);
  
  arb_t temp;
  arb_init(temp);
  
  arb_div(fdeg_jB, sinhB1m, coshB1m, prec);
  arb_div(temp, sinhB1p, coshB1p, prec);
  arb_sub(fdeg_jB, fdeg_jB, temp, prec);
  arb_mul(fdeg_jB, B0, fdeg_jB, prec);
  arb_div(fdeg_jB, fdeg_jB, B1, prec);

  arb_div(fdeg_jC, sinhC1p, coshC1p, prec);
  arb_div(temp, sinhC1m, coshC1m, prec);
  arb_sub(fdeg_jC, fdeg_jC, temp, prec);
  arb_mul(fdeg_jC, C0, fdeg_jC, prec);
  arb_div(fdeg_jC, fdeg_jC, C1, prec);

  arb_set(fdeg_j, A0);
  arb_add(fdeg_j, fdeg_j, fdeg_jB, prec);
  arb_add(fdeg_j, fdeg_j, fdeg_jC, prec);


  // gdeg(j)=
  //     D0
  //     +    E0/E1*sinh((j-njeq+JE)/E1)/(cosh((j-njeq+JE)/E1))
  //     -    E0/E1*sinh((j-njeq-JE)/E1)/(cosh((j-njeq-JE)/E1))

  arb_div(gdeg_jE, sinhE1p, coshE1p, prec);
  arb_div(temp, sinhE1m, coshE1m, prec);
  arb_sub(gdeg_jE, gdeg_jE, temp, prec);
  arb_mul(gdeg_jE, E0, gdeg_jE, prec);
  arb_div(gdeg_jE, gdeg_jE, E1, prec);

  arb_set(gdeg_j, D0);
  arb_add(gdeg_j, gdeg_j, gdeg_jE, prec);


  //yf(j) = yplane(fdeg(j))
  yplane(yf, fdeg, prec);
  
  //yg(j) = yplane(gdeg(j))
  yplane(yg, gdeg, prec);
  
  //y_f(j) = dy/dlat(lat=f(j)) 
  yplane_deg(y_f, fdeg, prec);

  //y_g(j) = dy/dlat(lat=g(j)) 
  yplane_deg(y_g, gdeg, prec);
  
  //yc_j(j) = d(0.5*gf - 0.5*yf)/dj = 0.5*(y_g*gdeg_j - y_f*fdeg_j);
  const slong  int2 = 2;
  arb_mul(yc_j, y_f, fdeg_j, prec);
  arb_neg(yc_j, yc_j);
  arb_addmul(yc_j, y_g, gdeg_j, prec);
  arb_div_si(yc_j, yc_j, int2, prec);

  //yr = (yf*yg)^(1/2)
  arb_mul(yr, yf, yg, prec);
  arb_sqrt(yr, yr, prec);
  
  //yphi_integrand(j) = -yc_j(j)/yr(j)
  arb_div(yphi_integrand, yc_j, yr, prec);
  arb_neg(yphi_integrand, yphi_integrand);
}

void MadecPlane::calc(const double jd){
  acb_t jc;
  acb_init(jc);  
  acb_set_d(jc, jd);
  MadecPlane::calc(jc);
}


void MadecPlane::yplane(arb_t res, arb_t deg, slong prec){
  //yplane(deg) = cot(pi*(0.25 + deg/360.0))
  const slong int360 = 360;
  const slong int4 = 4;
  arb_t quarter;
  arb_t pi_const;
  
  arb_init(quarter);
  arb_one(quarter);
  arb_div_si(quarter, quarter, int4, prec); 
  
  arb_init(pi_const);
  arb_const_pi(pi_const, prec);
  
  arb_div_si(res, deg, int360, prec); 
  arb_add(res, res, quarter, prec); 
  arb_mul(res, res, pi_const, prec);
  arb_cot(res, res, prec);
};


void MadecPlane::yplane_deg(arb_t res, arb_t deg, slong prec){
  //yplane_deg(deg) = -pi/360.0*(1.0+cot(pi*(0.25 + deg/360.0))^2)
  const slong int360 = 360;
  const slong int4 = 4;
  arb_t one, quarter;
  arb_t pi_const;
  arb_t cotan;

  arb_init(one);
  arb_one(one);
  
  arb_init(quarter);
  arb_div_si(quarter, one, int4, prec); 

  arb_init(pi_const);
  arb_const_pi(pi_const, prec);
  
  arb_init(cotan);
  arb_div_si(cotan, deg, int360, prec);
  arb_add(cotan, cotan, quarter, prec);
  arb_mul(cotan, cotan, pi_const, prec);
  arb_cot(cotan, cotan, prec);
  
  arb_mul(res, cotan, cotan, prec);
  arb_add(res, res, one, prec);
  arb_mul(res, res, pi_const, prec);
  arb_div_si(res, res, int360, prec);
  arb_neg(res, res);
};


int mp_integrate(acb_ptr res, const acb_t j, void * param, slong order, slong prec)
{
    if (order > 1)
        flint_abort();  /* Would be needed for Taylor method. */

    MadecPlane mp = *static_cast<MadecPlane*>(param); // dereference pointer
    mp.calc(j);
    acb_set_arb(res, mp.yphi_integrand);
    return 0;
}

void MadecPlane::integrate_phi_exp_arg(acb_ptr res, acb_ptr j1, MadecPlane mp){
  void* param = &mp;

  acb_t  j0;
  mag_t  tol;
  acb_calc_integrate_opt_t  options;
  slong  prec = mp.prec;
  slong  goal = prec;

  acb_init(j0);
  mag_init(tol);
  acb_calc_integrate_opt_init(options);

  
  acb_set_si(j0, madec::njeq); 
  mag_set_ui_2exp_si(tol, 1, -prec);

  acb_calc_integrate(res, mp_integrate, param, j0, j1, goal, tol, options, prec);
}

void MadecPlane::integrate_phi_exp_arg(acb_ptr res, const double j, MadecPlane mp){
  void* param = &mp;

  acb_t  j1;
  acb_init(j1);
  acb_set_d(j1, j);  

  MadecPlane::integrate_phi_exp_arg(res, j1, mp);
}

double MadecPlane::calc_integral(const double j, MadecPlane mp){
  void* param = &mp;

  MadecPlane::integrate_phi_exp_arg(mp.phi_exp_arg, j, mp);
  return to_double(mp.phi_exp_arg);
}



void MadecPlane::print(){
  std::cout << "  nj | " << to_double(nj) << std::endl;
  std::cout << "njeq | " << to_double(njeq) << std::endl;
  
  std::cout << "  A0 | " << to_string(A0, prec) << std::endl;  
  std::cout << "  A1 | " << to_string(A1, prec) << std::endl;

  std::cout << "  JB | " << to_string(JB, prec) << std::endl;
  std::cout << "  B1 | " << to_string(B1, prec) << std::endl;
  std::cout << "  B0 | " << to_string(B0, prec) << std::endl;

  std::cout << "  JC | " << to_string(JC, prec) << std::endl;
  std::cout << "  C1 | " << to_string(C1, prec) << std::endl;
  std::cout << "  C0 | " << to_string(C0, prec) << std::endl;

  std::cout << "  JE | " << to_string(JE, prec) << std::endl;
  std::cout << "  E1 | " << to_string(E1, prec) << std::endl;
  std::cout << "  E0 | " << to_string(E0, prec) << std::endl;

  std::cout << "  D0 | " << to_string(D0, prec) << std::endl;
  std::cout << "  D1 | " << to_string(D1, prec) << std::endl;

  std::cout << std::endl;
  std::cout << "fdeg | " << to_string(fdeg, prec) << std::endl;
  std::cout << "gdeg | " << to_string(gdeg, prec) << std::endl;

  std::cout << std::endl;
  std::cout << "fdeg_j | " << to_string(fdeg_j, prec) << std::endl;
  std::cout << "gdeg_j | " << to_string(gdeg_j, prec) << std::endl;

  std::cout << std::endl;
  std::cout << "  yf | " << to_string(yf, prec) << std::endl;
  std::cout << "  yg | " << to_string(yg, prec) << std::endl;

  std::cout << std::endl;
  std::cout << " y_f | " << to_string(y_f, prec) << std::endl;
  std::cout << " y_g | " << to_string(y_g, prec) << std::endl;

  std::cout << std::endl;
  std::cout << "yc_j | " << to_string(yc_j, prec) << std::endl;
  std::cout << "  yr | " << to_string(yr, prec) << std::endl;

  std::cout << std::endl;
  std::cout << "yphi_integrand | " << to_string(yphi_integrand, prec) << std::endl;
  
  
}



