function get_index(i,j,ni,nj)
  integer, intent(in):: i,j
  integer, intent(in):: ni,nj
  integer:: get_index
  
  get_index = nj*(i-1) + j;
end function


subroutine madec_lonlat(&
               iarr, jarr,&
               lon, lon_i, lon_j,&
               lat, lat_i, lat_j,&
               jspole, jnpole,&
               ni, nj)
  use iso_c_binding, only: c_double, c_int
  
  integer, external:: get_index
  integer, intent(in):: ni, nj
  real, intent(out):: iarr(ni), jarr(nj)
  real, dimension(ni, nj), intent(out):: lon, lon_i, lon_j
  real, dimension(ni, nj), intent(out):: lat, lat_i, lat_j
  real, intent(out):: jspole, jnpole
  
  real(c_double):: iarrc(ni), jarrc(nj)
  real(c_double), dimension(ni*nj):: lonc, lonc_i, lonc_j
  real(c_double), dimension(ni*nj):: latc, latc_i, latc_j
  real(c_double):: jspole_c, jnpole_c
  
  integer(c_int):: nic, njc
  integer:: i,j,n
  

  interface
    subroutine get_madec_lonlat(&
        iarr, jarr,&
        lon, lon_i, lon_j,&
        lat, lat_i, lat_j,&
        jspole, jnpole,&
        ni, nj&
    ) bind(C, name="get_madec_grid_")
      
      import :: c_double, c_int
      integer(c_int) :: ni, nj
      real(c_double), intent(out):: iarr(ni), jarr(nj)
      real(c_double), dimension(ni*nj):: lon, lon_i, lon_j
      real(c_double), dimension(ni*nj):: lat, lat_i, lat_j
      real(c_double), intent(out):: jspole, jnpole
    
    end subroutine
  end interface

  
  nic = ni
  njc = nj

  call get_madec_lonlat(&
      iarrc, jarrc,&
      lonc, lonc_i, lonc_j,&
      latc, latc_i, latc_j,&
      jspole_c, jnpole_c,&
      nic, njc)
  
  iarr = real(iarrc)
  jarr = real(jarrc)
  
  do j=1, nj
    do i=1,ni
      n = get_index(i,j,ni,nj)

      lon(i,j) = real(lonc(n))
      lon_i(i,j) = real(lonc_i(n))
      lon_j(i,j) = real(lonc_j(n))
      
      lat(i,j) = real(latc(n))
      lat_i(i,j) = real(latc_i(n))
      lat_j(i,j) = real(latc_j(n))
    end do
  end do

  jspole = jspole_c
  jnpole = jnpole_c

  

end subroutine
