find_package(ARB CONFIG QUIET)

if (ARB_FOUND)
  set(ARB_FOUND "${ARB_FOUND}")
  set(ARB_INCLUDE_DIRS "${ARB_INCLUDE_DIR}")
  set(ARB_LIBRARIES "${ARB_LIBRARIES}")
  set(ARB_VERSION "${ARBVersion}")

  if (NOT TARGET arb::arb)
    add_library(arb::arb INTERFACE IMPORTED)
  endif()

  set_target_properties(arb::arb PROPERTIES
    INTERFACE_LINK_LIBRARIES "arb::arb"
    INTERFACE_INCLUDE_DIRECTORIES "${ARB_INCLUDE_DIRS}")

else()

  include(FindPackageHandleStandardArgs)

  find_path(ARB_INCLUDE_DIRS arb.h)
  find_library(ARB_LIBRARIES arb)
  find_package_handle_standard_args(ARB REQUIRED_VARS ARB_INCLUDE_DIRS ARB_LIBRARIES)

  find_path(FLINT_INCLUDE_DIRS flint/flint.h)
  find_library(FLINT_LIBRARIES flint)
  if (FLINT_INCLUDE_DIRS AND FLINT_LIBRARIES)
    message("FLINT found")
    message("  FLINT_INCLUDE_DIRS: " ${FLINT_INCLUDE_DIRS})
    message("  FLINT_LIBRARIES: " ${FLINT_LIBRARIES})
  else()
    message(FATAL_ERROR "FLINT not found")
  endif()

  file(DOWNLOAD "https://raw.githubusercontent.com/git-for-windows/git-sdk-64/406d02f8f9dc8d0d744aa5698998b0bbee99f791/mingw64/include/mpfr.h" ${CMAKE_BINARY_DIR}/include/mpfr.h)

  add_library(arb::arb INTERFACE IMPORTED)
  set_target_properties(arb::arb PROPERTIES
    INTERFACE_LINK_LIBRARIES "${ARB_LIBRARIES};${FLINT_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${ARB_INCLUDE_DIRS};${FLINT_INCLUDE_DIRS};${CMAKE_CURRENT_BINARY_DIR}/include")
endif()

message("ARB_LIBRARIES: " ${ARB_LIBRARIES})
